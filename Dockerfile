FROM node
RUN mkdir /flatris
COPY package.json ./flatris
WORKDIR /flatris
RUN yarn install -production
COPY . /flatris
#WORKDIR /flatris
#RUN yarn install
RUN yarn test
RUN yarn build
CMD yarn start
